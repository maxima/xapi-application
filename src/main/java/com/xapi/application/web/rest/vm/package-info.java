/**
 * View Models used by Spring MVC REST controllers.
 */
package com.xapi.application.web.rest.vm;
